package rs.deuterium.rasp.dht22;

/**
 * Created by MilanNuke 21-Dec-19
 */
public class DHT22JsonDataBuilder {
    private String temperature;
    private String humidity;
    private String host;
    private String sensor;
    private String DEFAULT_SENSOR_TYPE = "DHT22";

    public DHT22JsonDataBuilder() {
    }

    public DHT22JsonDataBuilder jsonData() {
        return new DHT22JsonDataBuilder();
    }

    public DHT22JsonDataBuilder withTemperature(String temperature) {
        this.temperature = temperature;
        return this;
    }

    public DHT22JsonDataBuilder withHumidity(String humidity) {
        this.humidity = humidity;
        return this;
    }

    public DHT22JsonDataBuilder withHost(String host) {
        this.host = host;
        return this;
    }

    public DHT22JsonDataBuilder withSensor(String sensor) {
        this.sensor = sensor;
        return this;
    }

    public DHT22JsonDataBuilder withDefaultSensor() {
        this.sensor = DEFAULT_SENSOR_TYPE;
        return this;
    }

    public String build() {
        return "{\"temperature\":\"" + temperature + "\"," +
                "\"humidity\":\"" + humidity + "\"," +
                "\"host\":\"" + host + "\"," +
                "\"sensor\":\"" + sensor + "\"}";
    }


}
