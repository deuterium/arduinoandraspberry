package rs.deuterium.rasp.dht22;

import org.eclipse.paho.client.mqttv3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by MilanNuke 21-Dec-19
 */
public class MosquittoPublisher {

    private final Logger logger = LoggerFactory.getLogger(MosquittoPublisher.class);

    private final String mosquitoIp;
    private final String topic;
    private final String publisherId;
    private final IMqttClient publisher;
    private static final int CONNECTION_TIMEOUT = 10;

    public MosquittoPublisher(String mosquitoIp, String topic, String publisherId) throws MqttException {
        this.mosquitoIp = mosquitoIp;
        this.topic = topic;
        this.publisherId = publisherId;
        this.publisher = new MqttClient(mosquitoIp , publisherId);
        setOptionsAndConnect();
    }

    private void setOptionsAndConnect() throws MqttException {
        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(CONNECTION_TIMEOUT);
        publisher.connect(options);
    }

    public void disconnect() throws MqttException {
        this.publisher.disconnect();
    }

    public void publish(MqttMessage message) throws MqttException {
        message.setQos(0);
        message.setRetained(true);
        publisher.publish(this.topic, message);
        logger.debug("Message with id = {} and QoS = {} sent", message.getId(), message.getQos());
    }

}
