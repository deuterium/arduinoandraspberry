package rs.deuterium.rasp.dht22;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortMessageListener;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by MilanNuke 21-Dec-19
 */
public class MessageListener implements SerialPortMessageListener {

    private final Logger logger = LoggerFactory.getLogger(MessageListener.class);
    private String messageDelimiter = "***";
    private String sensorType = "DHT22";

    private DHT22RawDataParser parser;
    private MosquittoPublisher publisher;
    private InetAddress hostIp;

    public MessageListener(DHT22RawDataParser parser, MosquittoPublisher publisher) throws UnknownHostException {
        this.parser = parser;
        this.publisher = publisher;
        this.hostIp = InetAddress.getLocalHost();
        logger.info("IP address: {}", hostIp);
    }

    public byte[] getMessageDelimiter() {
        return messageDelimiter.getBytes();
    }

    public boolean delimiterIndicatesEndOfMessage() {
        return true;
    }

    public int getListeningEvents() {
        return SerialPort.LISTENING_EVENT_DATA_RECEIVED;
    }

    public void serialEvent(SerialPortEvent event) {

        byte[] delimitedMessage = event.getReceivedData();
        String message = new String(delimitedMessage);
        DHT22Data data = parser.parse(message.trim());

        if (data != null) {
            logger.info("Parsed: {}", data.toString());
            String json = toJson(data);
            try {
                publisher.publish(new MqttMessage(json.getBytes()));
                logger.debug("Message published: {}", json);
            } catch (MqttException e) {
                logger.error("Cannot send message: {}", json);
                e.printStackTrace();
            }
        }
    }

    private String toJson(DHT22Data data){
        return "{\"temperature\":\"" + data.getTemperature() + "\"," +
                "\"humidity\":\"" + data.getHumidity() + "\"," +
                "\"host\":\"" + hostIp.toString() + "\"," +
                "\"sensor\":\"" + sensorType + "\"}";
    }

    private String getJson(DHT22Data data){
        return  new DHT22JsonDataBuilder()
                .jsonData()
                .withHumidity(data.getHumidity().toString())
                .withTemperature(data.getTemperature().toString())
                .withDefaultSensor()
                .withHost(hostIp.toString())
                .build();
    }

    public void setMessageDelimiter(String messageDelimiter) {
        this.messageDelimiter = messageDelimiter;
    }

    public String getSensorType() {
        return sensorType;
    }

    public void setSensorType(String sensorType) {
        this.sensorType = sensorType;
    }
}
