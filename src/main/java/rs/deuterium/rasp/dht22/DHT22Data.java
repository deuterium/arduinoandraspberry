package rs.deuterium.rasp.dht22;

import java.math.BigDecimal;

/**
 * Created by MilanNuke 21-Dec-19
 */
public class DHT22Data {
    private BigDecimal temperature;
    private BigDecimal humidity;
    private BigDecimal sum;

    public DHT22Data() {
    }

    public DHT22Data(BigDecimal temperature, BigDecimal humidity, BigDecimal sum) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.sum = sum;
    }

    public BigDecimal getTemperature() {
        return temperature;
    }

    public void setTemperature(BigDecimal temperature) {
        this.temperature = temperature;
    }

    public BigDecimal getHumidity() {
        return humidity;
    }

    public void setHumidity(BigDecimal humidity) {
        this.humidity = humidity;
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return "DHT22Data[" +
                "temperature=" + temperature +
                ", humidity=" + humidity +
                ", sum=" + sum +
                ']';
    }
}
