package rs.deuterium.rasp.dht22;

import com.fazecast.jSerialComm.SerialPort;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.UnknownHostException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by MilanNuke 21-Dec-19
 */

public class DHT22SerialMain {

    private static final Logger logger = LoggerFactory.getLogger(DHT22SerialMain.class);
    private static final String MOSQUITTO_IP = "tcp://192.168.0.104:11883";
    private static final String TOPIC = "temperature/dht22";
    private static final String PUBLISHER_ID = "54f778e2-726c-4cb3-a7d9-d10239261a71";
    private static final String GET_DATA_COMMAND = "111";
//    private static final String HOST_IP = "http://192.168.0.101";

    public static void main(String[] args) throws MqttException, UnknownHostException {

        MosquittoPublisher publisher = new MosquittoPublisher(MOSQUITTO_IP, TOPIC, PUBLISHER_ID);
        DHT22RawDataParser parser = new DHT22RawDataParser();
        MessageListener listener = new MessageListener(parser, publisher);

        final SerialPort comPort = SerialPort.getCommPort("COM4");
        logger.info("Adding data listener...");
        comPort.addDataListener(listener);

        logger.info("Opening port...");
        comPort.openPort();

        Runnable task = () -> {
            comPort.writeBytes(GET_DATA_COMMAND.getBytes(), GET_DATA_COMMAND.length());
            logger.debug("GET_DATA_COMMAND send");
        };

        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleAtFixedRate(task, 3, 3, TimeUnit.SECONDS);

    }
}
