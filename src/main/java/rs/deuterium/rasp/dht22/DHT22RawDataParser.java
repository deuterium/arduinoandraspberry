package rs.deuterium.rasp.dht22;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * Created by MilanNuke 21-Dec-19
 */
public class DHT22RawDataParser {

    private static final Logger logger = LoggerFactory.getLogger(DHT22RawDataParser.class);
    private String messageDelimiter = "***"; // default value
    private String messageStartChar = "T"; // default value
    private int totalParts = 3; // default value

    public DHT22RawDataParser() {
    }

    public DHT22Data parse(String rawSensorData) {
        if (!rawSensorData.startsWith(messageStartChar)) {
            logger.error("Data is corrupted! Message did NOT start with: {}", messageStartChar);
            return null;
        }
        String rawData = rawSensorData.replace(messageDelimiter, "");
        String[] data = rawData.split("-");

        if (data.length != totalParts) {
            logger.error("Data is corrupted! Total data parts are not equal to: {}", totalParts);
            return null;
        }

        return parseData(data);
    }

    private DHT22Data parseData(String[] data) {
        BigDecimal temp = parseTemperature(data[0]);
        BigDecimal hum = parseHumidity(data[1]);
        BigDecimal sum = parseSum(data[2]);
        if((sum.compareTo(temp.add(hum)) != 0)){
            logger.error("Data is corrupted!");
            logger.error("Sum of temperature: {} and humidity: {} is NOT equal to: {}", temp, hum, sum);
        }
        return new DHT22Data(temp, hum, sum);
    }

    private BigDecimal parseTemperature(String data) {
        String temp = data.replace("T=", "");
        return parseToBigDecimal(temp);
    }

    private BigDecimal parseHumidity(String data) {
        String hum = data.replace("H=", "");
        return parseToBigDecimal(hum);
    }

    private BigDecimal parseSum(String data) {
        String sum = data.replace("S=", "");
        return parseToBigDecimal(sum);
    }

    private BigDecimal parseToBigDecimal(String data){
        try{
            return new BigDecimal(data);
        }catch (Exception e){
            logger.error("Error happened: {}", e.getMessage());
            logger.error("Data is corrupted! Cannot parse value: {} to BigDecimal", data);
            return new BigDecimal("-273");
        }
    }
}
