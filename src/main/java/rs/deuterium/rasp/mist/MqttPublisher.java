package rs.deuterium.rasp.mist;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by MilanNuke 31-Dec-19
 */
public class MqttPublisher {
    private static final Logger logger = LoggerFactory.getLogger(MqttPublisher.class);
    private IMqttClient mqttClient;

    public MqttPublisher(IMqttClient mqttClient) throws MqttException {
        this.mqttClient = mqttClient;
        mqttClient.connect(getOptions());
    }

    private static MqttConnectOptions getOptions(){
        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setConnectionTimeout(10);
        return options;
    }

    public void publish(MqttMessage message, String topic){
        if (!mqttClient.isConnected()) {
            logger.error("The server is not connected");
            return;
        }
        message.setQos(0);
        message.setRetained(true);
        try {
            mqttClient.publish(topic, message);
            logger.debug("Message sent at " + System.currentTimeMillis());
        } catch (MqttException e) {
            logger.error("There is some MqttException: {}", e.getMessage());
            e.printStackTrace();
        }
    }
}
