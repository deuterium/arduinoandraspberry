package rs.deuterium.rasp.dht22mqtt;

import com.fazecast.jSerialComm.SerialPort;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rs.deuterium.rasp.dht22.DHT22RawDataParser;

import java.net.UnknownHostException;

/**
 * Created by MilanNuke 27-Dec-19
 */
public class SendDataToMQTTMain {

    private static final Logger logger = LoggerFactory.getLogger(SendDataToMQTTMain.class);
    private static final String MOSQUITTO_IP = "tcp://192.168.0.104:11883";
    private static final String TEMPERATURE_TOPIC = "home/temperature/dht22";
    private static final String HUMIDITY_TOPIC = "home/humidity/dht22";
    private static final String PUBLISHER_ID_1 = "3ffce4af-fa52-4b50-98d6-59731c1b51ab";
    private static final String PUBLISHER_ID_2 = "c36bbb1c-299b-4f68-aab6-3e17edf0c16d";

    public static void main(String[] args) throws MqttException, UnknownHostException {

        MqttPublisher temperaturePublisher = new MqttPublisher(MOSQUITTO_IP, TEMPERATURE_TOPIC, PUBLISHER_ID_1);
        MqttPublisher humidityPublisher = new MqttPublisher(MOSQUITTO_IP, HUMIDITY_TOPIC, PUBLISHER_ID_2);
        DHT22RawDataParser parser = new DHT22RawDataParser();
        DHT22Listener listener = new DHT22Listener(parser, temperaturePublisher, humidityPublisher);

        final SerialPort comPort = SerialPort.getCommPort("COM5");
        logger.info("Adding data listener...");
        comPort.addDataListener(listener);

        logger.info("Opening port...");
        comPort.openPort();


    }
}
