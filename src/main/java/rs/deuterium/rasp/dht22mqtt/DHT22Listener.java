package rs.deuterium.rasp.dht22mqtt;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortMessageListener;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import rs.deuterium.rasp.dht22.DHT22Data;
import rs.deuterium.rasp.dht22.DHT22RawDataParser;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by MilanNuke 27-Dec-19
 */
public class DHT22Listener implements SerialPortMessageListener {

    private final Logger logger = LoggerFactory.getLogger(DHT22Listener.class);
    private String messageDelimiter = "***";
    private String sensorType = "DHT22";

    private DHT22RawDataParser parser;

    private MqttPublisher temperaturePublisher;
    private MqttPublisher humidityPublisher;
    private InetAddress hostIp;

    public DHT22Listener(DHT22RawDataParser parser, MqttPublisher tp, MqttPublisher hp) throws UnknownHostException {
        this.parser = parser;
        this.temperaturePublisher = tp;
        this.humidityPublisher = hp;
        this.hostIp = InetAddress.getLocalHost();
        logger.info("IP address: {}", hostIp);
    }

    public byte[] getMessageDelimiter() {
        return messageDelimiter.getBytes();
    }

    public boolean delimiterIndicatesEndOfMessage() {
        return true;
    }

    public int getListeningEvents() {
        return SerialPort.LISTENING_EVENT_DATA_RECEIVED;
    }

    public void serialEvent(SerialPortEvent event) {
        byte[] delimitedMessage = event.getReceivedData();
        String message = new String(delimitedMessage);
        DHT22Data data = parser.parse(message.trim());

        if (data != null) {
            logger.info("Parsed: {}", data.toString());
            try {
                temperaturePublisher.publish(new MqttMessage(temperatureToJsonBytes(data.getTemperature().toString())));
                humidityPublisher.publish(new MqttMessage(humidityToJsonBytes(data.getHumidity().toString())));
                logger.debug("Message published: {}", data);
            } catch (MqttException e) {
                logger.error("Cannot send message: {}", data);
                e.printStackTrace();
            }
        }
    }

    private byte[] temperatureToJsonBytes(String temperature){
        return toJson("temperature", temperature).getBytes();
    }

    private byte[] humidityToJsonBytes(String humidity){
        return toJson("humidity", humidity).getBytes();
    }

    private String toJson(String property, String value) {
        return "{\"host\":\"executor\"," +
                "\"sensor\":\"DHT22\"," +
                "\"property\":\"" + property + "\"," +
                "\"value\":\"" + value + "\"}";
    }
}
