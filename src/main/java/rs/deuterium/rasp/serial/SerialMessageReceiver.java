package rs.deuterium.rasp.serial;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortMessageListener;


/**
 * Created by MilanNuke 21-Dec-19
 */
public class SerialMessageReceiver {

    public static void main(String[] args) {
        SerialPort comPort = SerialPort.getCommPort("COM4");
        comPort.openPort();
        MessageListener listener = new MessageListener();
        comPort.addDataListener(listener);
//        try {
//            Thread.sleep(5000);
//        } catch (Exception e) { e.printStackTrace(); }
//        comPort.removeDataListener();
//        comPort.closePort();


    }

    private static final class MessageListener implements SerialPortMessageListener{

        public int getListeningEvents() {
            return SerialPort.LISTENING_EVENT_DATA_RECEIVED;
        }

        public byte[] getMessageDelimiter() {
            return "***".getBytes();
        }

        public boolean delimiterIndicatesEndOfMessage() {
            return true;
        }

        public void serialEvent(SerialPortEvent event){
            byte[] delimitedMessage = event.getReceivedData();
//            System.out.println("Received the following delimited message: " + new String(delimitedMessage));
            System.out.println(new String(delimitedMessage));
        }
    }
}
