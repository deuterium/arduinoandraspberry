package rs.deuterium.rasp.serial;

import com.fazecast.jSerialComm.SerialPort;

/**
 * Created by MilanNuke 21-Dec-19
 */
public class SerialSemiBlockingMain {

    public static void main(String[] args){
        SerialPort[] ports = SerialPort.getCommPorts();

        for (SerialPort port : ports){
            String portName = port.getSystemPortName();
            System.out.println(portName);
        }

        final SerialPort comPort = SerialPort.getCommPort("COM4");

        boolean openPort = comPort.openPort();

        if(openPort){
            System.out.println("Port is open");
        }else {
            System.out.println("Port is NOT open");
        }

        comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 1000, 0);
        try {
            while (true)
            {
                byte[] readBuffer = new byte[1024];
                int numRead = comPort.readBytes(readBuffer, readBuffer.length);
                System.out.println("Read " + numRead + " bytes.");
            }
        } catch (Exception e) { e.printStackTrace(); }
        comPort.closePort();
    }
}
