package rs.deuterium.rasp.serial;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;


/**
 * Created by MilanNuke on 12/20/2019
 *
 * http://fazecast.github.io/jSerialComm/
 * https://stackoverflow.com/questions/12317576/stable-alternative-to-rxtx/17966811
 * https://playground.arduino.cc/Interfacing/Java/
 * http://www.lediouris.net/RaspberryPI/Arduino/RPi.read.Arduino/readme.html
 *
 */
public class SerialMain {
    public static void main(String[] args) {

        SerialPort[] ports = SerialPort.getCommPorts();

        for (SerialPort port : ports){
            String portName = port.getSystemPortName();
            System.out.println(portName);
        }

        final SerialPort comPort = SerialPort.getCommPort("COM4");

        boolean openPort = comPort.openPort();

        if(openPort){
            System.out.println("Port is open");
        }else {
            System.out.println("Port is NOT open");
        }

//        comPort.addDataListener(new SerialPortDataListener() {
//
//            public int getListeningEvents() {
//                return SerialPort.LISTENING_EVENT_DATA_RECEIVED;
//            }
//
//            public void serialEvent(SerialPortEvent event){
//                byte[] newData = event.getReceivedData();
////                System.out.println("Received data of size: " + newData.length);
//                String data = new String(newData);
//                System.out.println(data);
//            }
//        });

        comPort.addDataListener(new SerialPortDataListener() {
            public int getListeningEvents() {
                return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
            }

            public void serialEvent(SerialPortEvent event) {
                if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
                    return;
                byte[] readBuffer = new byte[comPort.bytesAvailable()];
                comPort.readBytes(readBuffer, readBuffer.length);
                String result = new String(readBuffer);
                System.out.println(result);
            }
        });

        System.out.println("EOL");



    }
}
