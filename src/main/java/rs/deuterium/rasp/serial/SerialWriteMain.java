package rs.deuterium.rasp.serial;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;

import java.io.InputStream;

/**
 * Created by MilanNuke 21-Dec-19
 */
public class SerialWriteMain {
    public static void main(String[] args) throws InterruptedException {
        SerialPort[] ports = SerialPort.getCommPorts();

        for (SerialPort port : ports) {
            String portName = port.getSystemPortName();
            System.out.println(portName);
        }

        final SerialPort comPort = SerialPort.getCommPort("COM4");
        comPort.openPort();

        int bufferSize = comPort.getDeviceWriteBufferSize();
        System.out.println("device buffer size: " + bufferSize);

//        comPort.addDataListener(new SerialPortDataListener() {
//            public int getListeningEvents() {
//                return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
//            }
//
//            public void serialEvent(SerialPortEvent event) {
//                if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
//                    return;
//                byte[] readBuffer = new byte[comPort.bytesAvailable()];
//                comPort.readBytes(readBuffer, readBuffer.length);
//                String result = new String(readBuffer);
//                System.out.println(result);
//            }
//        });
//
//        for (int i = 0; i < 20; i++) {
//            Thread.sleep(2000);
//            String milan = "Milan";
//            comPort.writeBytes(milan.getBytes(), milan.length());
//        }
//
//        comPort.removeDataListener();
//        comPort.closePort();

        Thread.sleep(2000);
        String milan = "Milan";
        comPort.writeBytes(milan.getBytes(), milan.length());

        comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 0, 0);
        InputStream in = comPort.getInputStream();
        try {
            for (int j = 0; j < 1000; ++j)
                System.out.print((char) in.read());
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        comPort.closePort();
    }
}
